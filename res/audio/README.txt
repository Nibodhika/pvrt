Hello, because of the rules of the #linuxgamejam2017 the audios that should be contained here are not included in the release version (or the git for similar reasons). The game will run without the audios but  you'll get some warnings that you can ignore. If you wish to add audio to the game simply place the following files here

* ballHit.ogg - Will be played each time a ball hits a wall
* finishLine.ogg - Will play each time you sucessfully complete a line that does not pass the level
* levelPassed.ogg - Will play when you sucessfully complete a line that passes to the next level
* gameOver.ogg - Will be played when all lives are lost
* lifeLost.ogg - Will be played each time a life is lost
* main_music.ogg - Will be played as a loop in the BG
