#ifndef BALL_HPP
#define BALL_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "gameObject.hpp"


class Map;

class Ball :  public GameObject {
    
public:
    Ball(Map* board, sf::SoundBuffer& collision);
    ~Ball();
    
    void update(sf::Time time);
    
    float getRadius();

//     static sf::Sprite bleh;
//     sf::RenderTexture* render;

protected:
    virtual void doDraw(sf::RenderTarget &target, sf::RenderStates states) const;
    float speed_module = 200;
    
    sf::Vector2f _speed;
    float radius = 4;
    
private:
    Map* _board;
    sf::CircleShape _shape;
   std::vector<sf::Vertex> _trail;
   sf::Sound _collisionSound;
};


#endif
