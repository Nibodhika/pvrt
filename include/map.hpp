#ifndef MAP_HPP
#define MAP_HPP

#include <SFML/Graphics.hpp>
#include "gameObject.hpp"
#include "ball.hpp"

enum POINT_ADDED { NOT_ADDED, UPDATED_START_POINT, ADDED, CANCELLED_LINE, FINISHED_LINE };
enum PASS_LEVEL_BEHAIVOUR { RESET_LIVES, KEEP_LIVES, ADD_A_LIFE};

class Map : public GameObject {
public:
  Map(int balls, int width, int height);
  ~Map();
  
  void setBallsAndReset(int balls);
  
  void updateBoard();
  
  void clearPoints();
  POINT_ADDED addPoint(sf::Vector2f);
  void startPoints(sf::Vector2f);
  void finishPoints();
  
  sf::Sprite& getSprite();
  
  void cancel(bool takeLife = true);
  void clearTexture();

  int getWidth();
  int getHeight();
  
  bool isPointInside(sf::Vector2f point);
  
  bool isPointInside(sf::Vector2f point, float half_radius);
  
  void restart();
  void reset();
  
  void nextLevel();
  void restartGame();
  
  void update(sf::Time time);
  
  int points();
  float percentPoints();
  
  int lives();
  
  int currentLevel();
  bool levelPassed();
  bool gameOver();
  
  sf::Image getSubImage(sf::IntRect source);
  
private:
  virtual void doDraw (sf::RenderTarget &target, sf::RenderStates states) const;
  int _width;
  int _height;
  bool _finished;
  bool _cancelled;
  bool _gameOver = false;
   sf::RenderTexture _renderTexture;
   sf::Sprite _sprite;
   sf::Sprite _sprite2Draw;
   std::vector<sf::Vertex> _currentPoints;
//    sf::VertexArray _currentPoints;
   
   sf::Color lineColor = sf::Color::Blue;
   sf::Color _segmentedColor = sf::Color::Green;
   sf::Color _boardColor = sf::Color::Black;
   sf::Color _conqueredColor = sf::Color(255,255,255,0);
   
   sf::Vector2f _position;
   
  int floodFill(sf::Image& img, sf::Vector2f pos, sf::Color c);
  bool get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y,float p2_x, float p2_y, float p3_x, float p3_y, float *i_x = nullptr, float *i_y = nullptr);
   sf::Color clearColor;
   
   
   float dist_threshold2 = 4;
   bool points_are_distant(sf::Vector2f p1, sf::Vector2f p2);
   
   void eraseLines();
   
   int _points = 0;
   void calculatePoints();
   
   int _lives;
   int _max_lives = 5;
   
   PASS_LEVEL_BEHAIVOUR _pass_level_behaivour = ADD_A_LIFE;
   
   float _pass_level_points = 75;
   bool _levelPassed = false;
   
   std::vector<Ball> _balls;
   
   sf::Texture _texturebeforeLines;
   sf::Image _currentBoard;
   sf::Vector2f normalized(const sf::Vector2f&  in);

   
   sf::RectangleShape _outline;
   
   //Sounds
   sf::SoundBuffer _ballsCollision;
   
   sf::SoundBuffer _gameOverBuffer;
   sf::Sound _gameOverSound;
   
   sf::SoundBuffer _lifeLostBuffer;
   sf::Sound _lifeLostSound;
    
   sf::SoundBuffer _finishLineBuffer;
   sf::Sound _finishLineSound;
   
   sf::SoundBuffer _levelPassedBuffer;
   sf::Sound _levelPassedSound;
   
   sf::Music _music;
   
};

#endif
