#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <SFML/Graphics.hpp>

class GameObject : public sf::Transformable, public sf::Drawable {
public:
    GameObject(GameObject* parent = nullptr);
    ~GameObject();
    
    void update(sf::Time delta);
    
    sf::Transform getAbsoluteTransform() const;
    
protected:
    virtual void doDraw(sf::RenderTarget &target, sf::RenderStates states) const = 0;
      GameObject* _parent;
      
private:
    virtual void draw (sf::RenderTarget &target, sf::RenderStates states) const;
  
};

#endif
