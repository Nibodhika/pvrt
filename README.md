# About

PVRT is a game similar to the traditional Qix in which the player attempts to conquer part of the board.

The first prototype was created for the #LinuxGameJam17 in about two days, so if you value your sanity this is as far as you'll read into the code

# Compiling

## Dependencies

To compile this program you'll need SFML>=2, if you don't want or can't install sfml you can download the pre-compiled version on the tag

## Guide

Compiling it's straightforward

```
mkdir build
cd build
cmake ..
make
```

# Executing

```
cd build
./pvrt
```

# Rules

To play start slicing the board, the first click must always be on the outside of the board (already conquered areas count as outside).
From then move the mouse and bring the mouse to another point outside of the board to finish the line.
Whenever you complete a line each area that does not contain a ball gets taken away from the board, if you conquer over 75% of the board you pass to the next level, in which there's an aditional ball.
At any point you can release the mouse to cancel the line, but you'll get no points for it.
You have 5 lives, if a ball collides with the line you're drawing you lose a life.
If you draw over your own line you lose a life.
Each time you pass to the next level you win a life.

How far can you go?
