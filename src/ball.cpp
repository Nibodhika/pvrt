#include "ball.hpp"
#include <cmath>
#include "map.hpp"

// sf::Sprite Ball::bleh;

Ball::Ball(Map* board, sf::SoundBuffer& collision) : GameObject(board) {
    _board = board;
    
    _shape.setRadius(radius);
    _shape.setFillColor(sf::Color::Green);
    _shape.setOrigin(radius,radius); //Setting the origin to the center of the ball
    
    
    //Set a random speed
    float tmp = (float) rand() / (float) RAND_MAX;
    _speed.x = tmp;
    _speed.y = 1-tmp;
    _speed *= speed_module;
    
    //Randomizing initial position
    sf::Vector2f pos = getPosition();
    
    int dx = rand() % board->getWidth();
    int dy = rand() % board->getHeight();
    
    pos.x += dx;
    pos.y += dy;
    
     setPosition(pos);
     
     _collisionSound.setBuffer(collision);
     
//      render = new sf::RenderTexture();
}

Ball::~Ball()
{
}

#include <iostream>
void Ball::update(sf::Time time) {
    
    float delta = time.asSeconds();
    sf::Vector2f pos = getPosition();
    pos.x += _speed.x * delta;
    pos.y += _speed.y * delta;
    
    float half_radius = radius / 2.0;
    //Collision with board edges
     if(pos.x < half_radius ){
            pos.x = radius;
            _speed.x *= -1;
            _collisionSound.play();
        }
        else if(pos.x >= _board->getWidth() - half_radius){
            _speed.x *= -1;
            pos.x = _board->getWidth() -radius;
            _collisionSound.play();
        }
        if(pos.y < half_radius) {
            pos.y = radius;
            _speed.y *= -1;
            _collisionSound.play();
        }
        else if(pos.y >= _board->getHeight()+half_radius) {
            pos.y = _board->getHeight() - radius;
            _speed.y *= -1;
            _collisionSound.play();
        }
    
    //TODO melhorar isso
    
//     sf::IntRect rect(pos.x - radius, pos.y - radius,radius*2,radius*2);
//     sf::Image subimg = _board->getSubImage(rect);
//     sf::Texture tex;
//     tex.create(subimg.getSize().x,subimg.getSize().y);
//     tex.update(subimg);
//     sf::Sprite s(tex);
// 
//     render->create(radius*2, radius*2);
//     render->clear(sf::Color::White);
//     sf::CircleShape circle;
//     circle.setRadius(radius);
//     circle.setFillColor(sf::Color::Green);
//     render->draw(circle);
//     render->draw(s);
//     bleh.setTexture(render->getTexture(), true);
    
    
    
    if(! _board->isPointInside(pos) ){
        _speed.x *= -1;
        _speed.y *= -1;
        _collisionSound.play();
        pos = getPosition();
    }
    
    
//     Trail
    _trail.push_back(sf::Vertex(getPosition(), sf::Color::Cyan));
    if(_trail.size() > 50){
        _trail.erase(_trail.begin());
    }
    
    setPosition(pos);
    
}

void Ball::doDraw(sf::RenderTarget& target, sf::RenderStates states) const {
    sf::RenderStates lineState = states;
    lineState.transform = _parent->getAbsoluteTransform();
    
    target.draw(_shape,states);
    target.draw(&_trail[0], _trail.size(), sf::LinesStrip, lineState);
}

float Ball::getRadius() {
    return radius;
}

