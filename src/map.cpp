#include "map.hpp"
#include <cmath>

#include <iostream>
Map::Map(int balls, int width, int height) {
  _width = width;
  _height = height;
   if (!_renderTexture.create(_width, _height)) {
      // error...
    }


      setBallsAndReset(balls);
      
    _ballsCollision.loadFromFile("res/audio/ballHit.ogg");
    
    
    if( _gameOverBuffer.loadFromFile("res/audio/gameOver.ogg") )
         _gameOverSound.setBuffer(_gameOverBuffer);
    
    if( _lifeLostBuffer.loadFromFile("res/audio/lifeLost.ogg") )
        _lifeLostSound.setBuffer(_lifeLostBuffer);
    
    if(_finishLineBuffer.loadFromFile("res/audio/finishLine.ogg") )
         _finishLineSound.setBuffer(_finishLineBuffer);
    if(_levelPassedBuffer.loadFromFile("res/audio/levelPassed.ogg") )
         _levelPassedSound.setBuffer(_levelPassedBuffer);
    
    
    _music.setLoop(true);
    if( _music.openFromFile("res/audio/main_music.ogg") )
         _music.play();
    
    // get the target texture (where the stuff has been drawn)
    _sprite = sf::Sprite(_renderTexture.getTexture());
    _sprite2Draw = sf::Sprite(_renderTexture.getTexture());
    _lives = _max_lives;
    
    
    _outline.setOutlineColor(sf::Color::Yellow);
    int outlineThickness = 3;
    _outline.setOutlineThickness(outlineThickness);
    _outline.setFillColor(sf::Color::Transparent);
    _outline.setSize( sf::Vector2f(_width+outlineThickness-1,_height+outlineThickness-1));
    _outline.setPosition(-(outlineThickness -1)/2.0,-(outlineThickness-1)/2.0);
    
}
Map::~Map() {

}

void Map::setBallsAndReset(int balls){
    _balls.clear();
      for(int i = 0; i < balls; i++){
        _balls.push_back(Ball(this, _ballsCollision));
    }
    reset();
}

sf::Sprite& Map::getSprite() {
  return _sprite;
}

void Map::restart() {
_renderTexture.clear();
}

sf::Vector2f Map::normalized(const sf::Vector2f& v){
    sf::Vector2f out = v;
    float module = sqrt(out.x*out.x + out.y*out.y);
    out.x /= module;
    out.y /= module;
    return out;
}

void Map::updateBoard() {

  if(_currentPoints.size()>= 4) {
    
      int j = _currentPoints.size() - 2;
     for(int i = 0; i < _currentPoints.size() - 4;i++){
//        for(int j = i+2; j < _currentPoints.size() - 1;j++){
	 if( get_line_intersection(
	   _currentPoints[i].position.x, _currentPoints[i].position.y,
	   _currentPoints[i+1].position.x, _currentPoints[i+1].position.y,
	   _currentPoints[j].position.x,_currentPoints[j].position.y,
	   _currentPoints[j+1].position.x, _currentPoints[j+1].position.y)){
	     cancel();
	     return;
	  }
//        }
    }
 
 
    std::vector<sf::Vertex> lineVertex;
    float thickness = 3;
    if( _currentPoints.size() == 4) {
        sf::Vector2f p1 = _currentPoints[1].position;
        sf::Vector2f p0 = _currentPoints[0].position;
        
        sf::Vector2f line = p1 - p0;
        sf::Vector2f normal = normalized( sf::Vector2f( -line.y, line.x) );
       
        sf::Vector2f a = p0 - thickness * normal;
        sf::Vector2f b = p0 + thickness * normal;
        
        lineVertex.push_back(sf::Vertex(a, _currentPoints[0].color));
        lineVertex.push_back(sf::Vertex(b, _currentPoints[0].color));
    }
    
    for(int i = _currentPoints.size() - 4; i < _currentPoints.size() - 2; i++){
        
//          std::vector<sf::Vertex> lineVertex;
        
        sf::Vector2f p2 = _currentPoints[i+2].position;
        sf::Vector2f p1 = _currentPoints[i+1].position;
        sf::Vector2f p0 = _currentPoints[i].position;
        
        sf::Vector2f line = p1 - p0;
        sf::Vector2f normal = normalized( sf::Vector2f( -line.y, line.x) );
      
//         sf::Vector2f a = p0 - thickness * normal;
//         sf::Vector2f b = p0 + thickness * normal;
//         sf::Vector2f c = p1 - thickness * normal;
//         sf::Vector2f d = p1 + thickness * normal;
        
        sf::Vector2f dp1p0 = normalized( (p1-p0) );
        
         sf::Vector2f dp2p1 = normalized( (p2-p1) );
        
        sf::Vector2f tangent = normalized( (dp2p1 + dp1p0) );

        sf::Vector2f miter = sf::Vector2f( -tangent.y, tangent.x ); // tangent normal
        float mitter_dot_normal = miter.x * normal.x + miter.y * normal.y;
        float length = thickness / mitter_dot_normal;

        sf::Vector2f c = p1 - length * miter;
        sf::Vector2f d = p1 + length * miter;
        

        
        
//         lineVertex.push_back(sf::Vertex(a, _currentPoints[i].color));
//         lineVertex.push_back(sf::Vertex(b, _currentPoints[i].color));
        
        lineVertex.push_back(sf::Vertex(c, _currentPoints[i+1 ].color));
        lineVertex.push_back(sf::Vertex(d, _currentPoints[i+1].color));
        
//         _renderTexture.draw(&lineVertex[0],  lineVertex.size(), sf::Quads);
        
    }


        sf::Vector2f p1 = _currentPoints[_currentPoints.size() - 2].position;
        sf::Vector2f p0 = _currentPoints.back().position;
        
        sf::Vector2f line = p1 - p0;
        sf::Vector2f normal = normalized( sf::Vector2f( -line.y, line.x) );
       
        sf::Vector2f a = p0 - thickness * normal;
        sf::Vector2f b = p0 + thickness * normal;
        
        lineVertex.push_back(sf::Vertex(a, _currentPoints.back().color));
        lineVertex.push_back(sf::Vertex(b, _currentPoints.back().color));
    
    
    
    _renderTexture.draw(&lineVertex[0],  lineVertex.size(), sf::TrianglesStrip);

     _renderTexture.draw(&_currentPoints[0], _currentPoints.size(), sf::LinesStrip);
  }
  
    
 _renderTexture.display();
}


int Map::lives() {
    return _lives;
}


int Map::points() {
    return _points;
}

float Map::percentPoints(){
    return (_points / (float) (_width*_height) ) * 100;
}

void Map::calculatePoints() {
    int points = 0;
    for(int i = 0; i < _width; i++)
        for(int j = 0; j < _height; j++)
            if(_currentBoard.getPixel(i,j).a < 255)
                points++;
            
    _points = points;
}


void Map::clearTexture() {

//   std::cout << "clearColor is: " << (int) clearColor.r << "," << (int) clearColor.g << "," << (int) clearColor.b << std::endl;
  
  int count = 0;
//     img = _renderTexture.getTexture().copyToImage();
   
   sf::Color first = _currentBoard.getPixel(0,0);
//      std::cout << "first is: " << (int) first.r << "," << (int) first.g << "," << (int) first.b << std::endl;
   
   for(int i = 0; i < _width; i++){
     for(int j = 0; j < _height; j++){
         sf::Color pixel_color = _currentBoard.getPixel(i,j);
         if(pixel_color == _segmentedColor){
             _currentBoard.setPixel(i,j,_boardColor);
        }
        else{
             _currentBoard.setPixel(i,j,_conqueredColor);
        }
    }
  }
  
   sf::Texture* tex = new sf::Texture();
  tex->loadFromImage(_currentBoard);
  _sprite.setTexture(*tex);
  

  _renderTexture.clear(sf::Color::Transparent);
   _renderTexture.draw(_sprite);
  
    _currentBoard = _renderTexture.getTexture().copyToImage();
    tex->loadFromImage(_currentBoard);
    _sprite.setTexture(*tex);
//   _renderTexture.clear();
  
}

bool Map::isPointInside(sf::Vector2f point) {
  return point.x >= 0 && point.x < _width &&
    point.y >= 0 && point.y < _height && 
    _currentBoard.getPixel(point.x,point.y).a == 255;
}

bool Map::isPointInside(sf::Vector2f point, float half_radius) {

    sf::Vector2f p1 = point;
    for(p1.y = point.y - half_radius;p1.y< point.y+half_radius; p1.y++)
        for(p1.x = point.x - half_radius;p1.x < point.x+half_radius; p1.x++){
            if ( ! isPointInside(p1) ){
                return false;
            }
        }
    return true;
}


POINT_ADDED Map::addPoint(sf::Vector2f point) {
     if(_cancelled || _finished)
            return NOT_ADDED;
     
//      sf::Image img = _renderTexture.getTexture().copyToImage();
      sf::Image before = _texturebeforeLines.copyToImage();
     bool inside = isPointInside( point);
     
     if(_currentPoints.size() == 0) { //In case this is the first point
            if(inside) {
                std::cout << "Points: " <<  "First point inside" << std::endl;
                cancel();
                return CANCELLED_LINE;
            }
            else{
                _currentPoints.push_back(sf::Vertex(point, lineColor));
            }
        }
        else if(_currentPoints.size() == 1){ //Only added one point, need to update it
            if(inside){ //point is inside, start adding points
                 _currentPoints.push_back(sf::Vertex(point, lineColor));
                updateBoard();
            }
            else{ //point is outsisde, update first point
                _currentPoints[0].position.x = point.x;
                _currentPoints[0].position.y = point.y;
                return UPDATED_START_POINT;
            }
        }
        else{ //Line already started to be filled
            if(inside){ //Just keep adding points
                    sf::Vector2f& p = _currentPoints.back().position;
                    if(  points_are_distant(point, p)  ) {
                    //if(p2.x != p.x || p2.y != p.y) {
                         _currentPoints.push_back(sf::Vertex(point, lineColor));
                        updateBoard();
                    }
            }
            else{ //Finish the line
//                 std::cout << "Points: " <<  "Finishing " << point.x << "," << point.y  << " is outside" << std::endl;
                 _currentPoints.push_back(sf::Vertex(point, lineColor));
                updateBoard();
                finishPoints();
                return FINISHED_LINE;
            }
        }
        return ADDED;
}

void Map::startPoints(sf::Vector2f point) {
//   std::cout << "Starting" << std::endl;
   _currentPoints.push_back(sf::Vertex(point, lineColor));
   _finished = false;
   _cancelled = false;
   
//    sf::Image img = _renderTexture.getTexture().copyToImage();
   if(isPointInside( point)){
//      std::cout << "StartPoint inside image" << std::endl;
       cancel(false); //Cancel but don't take a life if the first point was inside
       return;
    }
    
    _texturebeforeLines.loadFromImage(_currentBoard);
   
}

void Map::cancel(bool takeLife) {
    if(_cancelled || _finished)
        return;
      _cancelled = true;
      clearPoints();
      eraseLines();
      if(takeLife) {
        _lives--;
        if(_lives == 0){
            _gameOver = true;
            _music.stop();
            _gameOverSound.play();
        }
        else{
            _lifeLostSound.play();
        }
      }
}

void Map::eraseLines(){
     sf::Sprite beforeLines(_texturebeforeLines);
     _renderTexture.draw(beforeLines);
}


int Map::floodFill(sf::Image& img, sf::Vector2f pos, sf::Color c){
  
  std::vector<sf::Vector2f> positions;
  positions.push_back(pos);
  img.setPixel(pos.x,pos.y,c);
  
  int cont = 0;
  while(positions.size() > 0) {
    cont++;
    sf::Vector2f current = positions.back();
     positions.pop_back();
     
//       std::cout << "Current is: " << current.x << "," << current.y << std::endl;
//       std::cout << "there are " << positions.size() << " elements" << std::endl;
     
  sf::Vector2f up(current.x, current.y -1);
  sf::Vector2f down(current.x, current.y +1);
  sf::Vector2f left(current.x -1, current.y);
  sf::Vector2f right(current.x +1, current.y);
  
  if(up.y >=0 && img.getPixel(up.x,up.y) == _boardColor){
    positions.push_back(up);
    img.setPixel(up.x,up.y,c);
//     std::cout << "Adding up: " << up.x << "," << up.y << std::endl;
  }
  if(down.y < img.getSize().y && img.getPixel(down.x,down.y) == _boardColor){
    positions.push_back(down);
    img.setPixel(down.x,down.y,c);
//     std::cout << "Adding down: " << down.x << "," << down.y  << std::endl;
  }
  if(left.x >= 0 && img.getPixel(left.x,left.y) == _boardColor){
    positions.push_back(left);
    img.setPixel(left.x,left.y,c);
//     std::cout << "Adding left: " << left.x << "," << left.y  << std::endl;
  }
  if(right.x < img.getSize().x && img.getPixel(right.x,right.y) == _boardColor){
    positions.push_back(right);
    img.setPixel(right.x,right.y,c);
//     std::cout << "Adding right: " << right.x << "," << right.y  << std::endl;
  }
  
  }
  
  return cont;
}

bool Map::get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y, 
    float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
{
    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    float s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return true;
    }

    return false; // No collision
}

void Map::nextLevel() {
    setBallsAndReset(_balls.size() + 1);
    _levelPassed = false;
}

void Map::restartGame() {
    _gameOver = false;
     setBallsAndReset(1);
      _lives = _max_lives;
     _music.play();
}

int Map::currentLevel() {
    return _balls.size();
}

bool Map::levelPassed() {
    return _levelPassed;
}

bool Map::gameOver() {
    return _gameOver;
}

void Map::finishPoints() {
    if(_finished || _cancelled)
        return;
  _finished = true;
  
  
//   sf::Image img = _renderTexture.getTexture().copyToImage();
   _currentBoard = _renderTexture.getTexture().copyToImage();
  for(int i = 0; i < _balls.size(); i++){
        sf::Vector2f pos = _balls[i].getPosition();
        if(_currentBoard.getPixel(pos.x, pos.y) == _boardColor){ //This ball is not segmented
             int n = floodFill(_currentBoard, pos,_segmentedColor);
//              std::cout << n << " elements" << std::endl;
        }
  }
  
   clearTexture();
  
  clearPoints();
  calculatePoints();
  
  if(percentPoints() > _pass_level_points){
      _levelPassedSound.play();
      _levelPassed = true;
  }
  else
      _finishLineSound.play();
 
//   _renderTexture.draw(s);
    _currentBoard = _renderTexture.getTexture().copyToImage();
    _texturebeforeLines.loadFromImage(_currentBoard);
  
}

void Map::clearPoints() {
  _currentPoints.clear();
}


void Map::doDraw(sf::RenderTarget& target, sf::RenderStates states) const {

     target.draw(_outline,states);
    target.draw(_sprite2Draw, states);

    for(const Ball& b : _balls){
        target.draw(b, states);
    }
    
   

}

void Map::reset() {
    _cancelled = false;
    _finished = false;
    switch(_pass_level_behaivour){
        case RESET_LIVES:
        _lives = _max_lives;
        break;
        case ADD_A_LIFE:
            _lives++;
            break;
        case KEEP_LIVES:
            break;
    }
    _renderTexture.clear();
    _points = 0;
    _currentBoard = _renderTexture.getTexture().copyToImage();
    _texturebeforeLines.loadFromImage(_currentBoard);
}

bool Map::points_are_distant(sf::Vector2f p1, sf::Vector2f p2) {
    float dx = p1.x - p2.x;
    float dy = p1.y - p2.y;
    float dist = dx*dx + dy*dy;
    return dist > dist_threshold2;
}


void Map::update(sf::Time time) {
    
    sf::Image img = _renderTexture.getTexture().copyToImage();
    
    for(Ball& b : _balls){
        b.update(time);
        
        //Check if ball collided with current line
        
        sf::Vector2f pos = b.getPosition();
        float half_radius = b.getRadius() / 2;
        bool breakFors = false;
        for(int j = pos.y - half_radius; j < pos.y + half_radius; j++) {
            if(breakFors)
                break;
            for(int i = pos.x - half_radius; i < pos.x + half_radius; i++){
                if(breakFors)
                    break;
                if(i < 0 || i > _width || j < 0 || j > _height)
                    continue;
                if(img.getPixel(i,j) == lineColor){
                    cancel();
                    breakFors = true;
                }
            }
        }
        
    }
}

int Map::getWidth() {
    return _width;
}


int Map::getHeight() {
    return _height;
}


sf::Image Map::getSubImage(sf::IntRect source) {
    sf::Image out;
    out.create(source.width, source.height,sf::Color::Black);
    out.copy(_currentBoard,0,0,source);
    return out;
}

