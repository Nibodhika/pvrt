#include <SFML/Graphics.hpp>
#include <iostream>

#include "map.hpp"

int main()
{
    // create the window
    sf::RenderWindow window(sf::VideoMode(800, 800), "PV=RT",sf::Style::None);

    srand(time(NULL));
    
    Map board(1, 500,500);
//     Board board(1,500,500);
    sf::Vector2f pos(100,100);
    board.setPosition(pos);

    bool clicked = false;
    sf::Clock clock;
    
    
    sf::Font arial;
    if (!arial.loadFromFile("res/fonts/arial.ttf"))
{
    std::cout << "Could not load from file" << std::endl;
}


    float percent_points = 0;
    sf::Text pointsText;
    pointsText.setFont(arial);
    // set the character size
    pointsText.setCharacterSize(24); // in pixels, not points!
    // set the color
    pointsText.setFillColor(sf::Color::Red);
    // set the text style
//     pointsText.setStyle(sf::Text::Bold | sf::Text::Underlined);
    pointsText.setString("Board conquered: " + std::to_string((int) percent_points) + "%");
    
     sf::Text livesText;
      livesText.setFont(arial);
     livesText.setPosition(0,30);
     livesText.setCharacterSize(24); // in pixels, not points!
    livesText.setFillColor(sf::Color::Green);
     livesText.setString("Lives: " + std::to_string(board.lives()));
    
    sf::Text levelText;
      levelText.setFont(arial);
     levelText.setPosition(300,700);
     levelText.setCharacterSize(24); // in pixels, not points!
    levelText.setFillColor(sf::Color::Green);
    levelText.setString("Level: " + std::to_string(board.currentLevel()));
    
     sf::Text gameOverText;
      gameOverText.setFont(arial);
     gameOverText.setPosition(160,300);
     gameOverText.setCharacterSize(36); // in pixels, not points!
    gameOverText.setFillColor(sf::Color::Red);
    gameOverText.setString("         Game Over\nPress Space to restart");
    
    
    float next_level_points = 75;
    sf::Time safe_frame = sf::seconds(1/60.0);
    // run the program as long as the window is open
     sf::Time elapsed = clock.restart();
     
     
     
     //My cursor
     window.setMouseCursorVisible(false); // Hide cursor
     sf::RenderTexture mouseTexture;
     mouseTexture.create(3,3);
     mouseTexture.clear(sf::Color::Red);
     sf::Sprite pointer(mouseTexture.getTexture());
     pointer.setOrigin(1,1);
     
    while (window.isOpen())
    {
        

//         elapsed += clock.restart();
          sf::Time elapsed = clock.restart();
        
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
	
// 	sf::Vector2i position_i = sf::Mouse::getPosition(window);
//         sf::Vector2f position_f = window.mapPixelToCoords(position_i);
	
        bool levelPassed = board.levelPassed();
        bool gameOver = board.gameOver();
        
        livesText.setString("Lives: " + std::to_string(board.lives()));
        
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();
            else if(event.type == sf::Event::KeyPressed){
                if(event.key.code == sf::Keyboard::Space){
                    if(levelPassed) {
                        board.nextLevel();
                    }
                    else if(gameOver){
                        board.restartGame();
                    }
                    
                    if(levelPassed || gameOver){
                        clicked = false;
                        pointsText.setString("Board conquered: " + std::to_string((int) board.percentPoints()) + "%");
                        levelText.setPosition(300,700);
                        levelText.setString("Level: " + std::to_string(board.currentLevel()));
                    }
                }
            }
            else if (! levelPassed && !gameOver) { //Handle in-game input, do nothing if level was already beaten
                if (event.type == sf::Event::MouseButtonPressed) { //If clicked start new points
                    board.clearPoints();
                    sf::Vector2f position_f = window.mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
                    board.startPoints(sf::Vector2f(position_f.x - pos.x, position_f.y - pos.y));
                    clicked = true;
                }
                else if(clicked && event.type == sf::Event::MouseMoved) { //If already clicked and moved pass new points
                    sf::Vector2f position_f = window.mapPixelToCoords(sf::Vector2i(event.mouseMove.x, event.mouseMove.y));
                    POINT_ADDED status = board.addPoint(sf::Vector2f(position_f.x - pos.x,position_f.y - pos.y));
                    if(status == POINT_ADDED::FINISHED_LINE){ //If this point finished the line
                         clicked = false;
                         //Check if beated level
                         percent_points = board.percentPoints();
                         pointsText.setString("Board conquered: " + std::to_string((int) percent_points) + "%");
                        if(percent_points > next_level_points) {
                            //Go to next level
                            levelPassed = true;
                            levelText.setPosition(230,700);
                            levelText.setString("Press space for level " + std::to_string(board.currentLevel() + 1));
                        }
                    }
                }
                else if(clicked && event.type == sf::Event::MouseButtonReleased){ //if released cancel the current line
                    clicked = false;
                    board.cancel(false); //Releasing the mouse does not take a life
                }
            } //!levelPassed
        } //!while events

        
        pointer.setPosition(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window)));
        
        
        // clear the window with black color
        window.clear(sf::Color(50,0,50));

        
        if( ! levelPassed && !gameOver) { 
//             int simulated_frames = 0;
//             while(elapsed > safe_frame){
//                 simulated_frames++;
//                  board.update( safe_frame);
//                  elapsed -= safe_frame;
//             }
//             if(simulated_frames > 1)
//                 std::cout << "Simulated " << simulated_frames << " frames " << std::endl;
            board.update( elapsed);
        }
        
        
        // draw everything here...
        window.draw(board);
        
        window.draw(levelText);
        window.draw(pointsText);
        window.draw(livesText);
	
        if(gameOver)
            window.draw(gameOverText);
        
//         window.draw(Ball::bleh);
        
         window.draw(pointer);
        // end the current frame
        window.display();
    }

    return 0;
}
