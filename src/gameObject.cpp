#include "gameObject.hpp"

GameObject::GameObject(GameObject* parent){
    _parent = parent;
}

GameObject::~GameObject(){
    
}

sf::Transform GameObject::getAbsoluteTransform() const {
    sf::Transform t;
    if(_parent)
        t = _parent->getTransform() *  getTransform();
    else
        t = getTransform();
    return t;
}


void GameObject::update(sf::Time delta) {
}

void GameObject::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform = getAbsoluteTransform();
    doDraw(target, states);
}
